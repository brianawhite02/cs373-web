import requests
import pandas as pd
def apiconnect():
    headers = {
        'X-API-Key': 'aLPU1BjfQphfOWjE0Z7Y1tB2pRRvGJl5loZe37U3',
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }

    response = requests.get('https://api.propublica.org/congress/v1/115/house/members.json', headers=headers)

    if response.status_code == 200:
        return response.json()
    return False

data = apiconnect()

house = data['results'][0]['members']
df = pd.DataFrame (house)
# df
# df = df.drop('at_large', axis = 1)
# df = df.drop('fec_candidate_id', axis =1)
# df = df.drop('fax', axis = 1)
# df = df.drop('cspan_id', axis = 1)
# df = df.drop('dw_nominate', axis = 1)
# df = df.drop('crp_id', axis = 1)
# df = df.drop('contact_form', axis = 1)
# df = df.drop('geoid', axis = 1)
# df = df.drop('ocd_id', axis = 1)
# df = df.drop('google_entity_id', axis = 1)
# df = df.drop('govtrack_id', axis = 1)
# df = df.drop('ideal_point', axis = 1)
# df = df.drop('icpsr_id', axis = 1)
# df = df.drop('rss_url', axis = 1)
# df = df.drop('seniority', axis = 1)
# df = df.drop('last_updated', axis = 1)
# df = df.drop('leadership_role', axis = 1)
# df = df.drop('votesmart_id', axis = 1)
# df = df.drop('id', axis = 1)
# df = df.drop('short_title', axis = 1)
# df = df.drop('api_uri', axis = 1)
# df = df.drop('suffix', axis = 1)
# df = df.drop('youtube_account', axis = 1)
# df = df.drop('middle_name', axis = 1)

df = df[['first_name', 'last_name', 'party', 'state', 'district', 'votes_with_party_pct', 'office', 'twitter_account', 'title', 'facebook_account', 'phone', 'url']]
df.iloc[0]